package com.human.lw7;

import android.os.AsyncTask;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

/**
 * Created by human on 10.04.18.
 */

public class BackgroundTask extends AsyncTask<Double, Void, Double[]> {

    MainActivity mainActivity;

    public BackgroundTask(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    protected void onPreExecute() {

        mainActivity.showProgressBar();
    }

    @Override
    protected Double[] doInBackground(Double... doubles) {
        Double max = doubles[0];
        Double sum = 0d;
        boolean negativeMatched = false;
        for (Double aDouble: doubles) {
            max = Math.max(max, aDouble);

            if (!negativeMatched && aDouble > 0) {
                sum += aDouble;
            }
            else {
                negativeMatched = true;
            }
            try {
              //  wait(50);
            }
            catch (Exception e) {

            }
        }

        return new Double[] {max, sum};
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);

    }

    @Override
    protected void onPostExecute(Double[] aDouble) {
        mainActivity.hideProgressBar();
        mainActivity.setResults(aDouble);
    }
}
