package com.human.lw7;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    BackgroundTask backgroundTask;
    Double[] doubles;
    LinearLayout mainLinearLayout;
    ProgressBar progressBar;



    public TextView textViewMax;
    public TextView textViewSum;
    Double[] answers;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainLinearLayout = (LinearLayout) findViewById(R.id.mainLinearLayout);
        //progressBar = (ProgressBar) findViewById(R.id.progressBar);
        textViewMax = (TextView) findViewById(R.id.textViewMax);
        textViewSum = (TextView) findViewById(R.id.textViewSum);


        backgroundTask = new BackgroundTask(this);
        doubles = new Double[20];
        doubles = fillArray(doubles);
        backgroundTask.execute(doubles);
    }


    public Double[] fillArray(Double[] doubles) {
        Random random = new Random();
        for (int i = 0; i < doubles.length; i++) {
            doubles[i] = random.nextDouble();
        }
        return doubles;
    }

    public void showProgressBar() {
        //progressBar.setVisibility(ProgressBar.VISIBLE);
    }

    public void hideProgressBar() {
        //progressBar.setVisibility(ProgressBar.GONE);
    }

    public void setResults(Double[] results) {
        textViewMax.setText("Max: " + String.valueOf(doubles[0]));
        textViewSum.setText("Sum: " + String.valueOf(doubles[1]));
    }

    public void goToSecondPage(View view) {
        Intent intent = new Intent(getApplicationContext(), PicturesActivity.class);
        startActivity(intent);
    }
}
