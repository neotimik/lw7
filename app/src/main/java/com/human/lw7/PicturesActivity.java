package com.human.lw7;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

public class PicturesActivity extends AppCompatActivity {

    LinearLayout linearLayout;
    DownloadTask downloadTask;
    ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pictures);
        linearLayout = (LinearLayout)findViewById(R.id.linearLayout);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        downloadTask = new DownloadTask(this);
        downloadTask.execute(3);
    }

    public void showImages(Bitmap[] bitmaps) {
        int n = bitmaps.length;
        for(int i = 0; i < n; i++) {
            ImageView imageView = new ImageView(this);
            imageView.setImageBitmap(bitmaps[i]);
            /*ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) imageView.getLayoutParams();       // how to set margin ??
            marginParams.setMargins(0, 5, 0, 0);
            imageView.setLayoutParams(marginParams);
            */
            linearLayout.addView(imageView);
        }

    }

    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }



}
