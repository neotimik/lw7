package com.human.lw7;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.InputStream;
import java.net.URL;

/**
 * Created by human on 15.04.18.
 */

public class DownloadTask extends AsyncTask<Integer, Void, Bitmap[]> {

    PicturesActivity picturesActivity;


    public DownloadTask(PicturesActivity picturesActivity) {
        this.picturesActivity = picturesActivity;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Bitmap[] doInBackground(Integer... params) {
        Bitmap[] bitmaps = new Bitmap[params[0]];
        String url = "https://source.unsplash.com/random";
        publishProgress();
        long delay = 0;
        try {
            for (int i = 0; i < params[0]; i++) {
                Thread.sleep(delay);                                                  //why not wait?
                bitmaps[i] = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
                delay = 1000;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmaps;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        picturesActivity.showProgressBar();
    }

    @Override
    protected void onPostExecute(Bitmap[] bitmaps) {
        picturesActivity.hideProgressBar();
        picturesActivity.showImages(bitmaps);
    }
}
